var gulp = require('gulp'),
    browserify = require('gulp-browserify'),
    babelify = require("babelify"),
    jadeify = require('jadeify'),
    jade = require('gulp-jade'),
    replace = require('gulp-replace'),
    cssify = require('cssify'),
    manifest = require('gulp-manifest');

var appSrc = 'src/app.jsx',
    appOutput = './build/';

var connect = require('gulp-connect'),
    watch = require('gulp-watch'),
    sass = require('gulp-sass');
    importCss = require('gulp-import-css');

var exec = require('child_process').exec;

var gutil = require('gulp-util');
var livereload = require('gulp-livereload');
var http = require('http');
var ecstatic = require('ecstatic');

var autoprefixer = require('gulp-autoprefixer');

var path = function (rel) {
    return './src/' + rel;
}

gulp.task('webserver', function () {
    connect.server({
        livereload: true,
        root: ['build']
    });
});

gulp.task('livereload', function () {
    gulp.src(['build/styles/*.css', 'build/scripts/*.js'])
        .pipe(watch([
            'build/*.css',
            'build/*.js',
            'build/*.html'
        ]))
        .pipe(connect.reload());
});

gulp.task('browserify', function () {
    // Single entry point to browserify
    gulp.src(appSrc)
        .pipe(browserify({
            transform: [jadeify, babelify.configure({
                //blacklist: ["generators"]
            }), cssify],
            extensions: ['.js'],
            insertGlobals: true,
            debug: false
        }))
        .pipe(gulp.dest(appOutput))
        .pipe(gulp.dest('./www/js'))
});


gulp.task('manifest', function(){
  gulp.src(['www/*','www/**/*'])
    .pipe(manifest({
      hash: true,
      preferOnline: true,
      network: ['http://*', 'https://*', '*'],
      cache: [
	      'http://fonts.googleapis.com/css?family=Open+Sans:300,600,400&subset=latin,latin-ext',
	      'https://fontastic.s3.amazonaws.com/4W99YizmV7sJGMZLSxZnzM/icons.css'
      ],
      filename: 'app.manifest',
      exclude: 'app.manifest'
     }))
    .pipe(gulp.dest('www'));
});


gulp.task('templates', function () {
    var YOUR_LOCALS = {};

    gulp.src('./src/*.jade')
        .pipe(jade({
            locals: YOUR_LOCALS
        }))
        .pipe(gulp.dest(appOutput))
        .pipe(replace("href=\"./main.css\"", "href=\"./css/main.css\""))
        .pipe(replace("src=\"./app.js\"", "src=\"./js/app.js\""))
        .pipe(gulp.dest('./www/'))
});

gulp.task('sass', function () {
    gulp.src('src/main.scss')
        .pipe(sass())
        .pipe(importCss())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(appOutput))
        .pipe(gulp.dest('./www/css'));
});

gulp.task('watch', function () {
    gulp.watch([path('*.scss'), path('**/*.scss')], ['sass', 'manifest']);
    gulp.watch([path('*.js'), path('**/*.js'), path('**/*.jade'), path('*.jsx'), path('**/*.jsx')], ['browserify', 'manifest']);
    gulp.watch([path('*.jade')], ['templates', 'manifest']);
});

var watchGlob = 'www/**/*.{js,html,css,manifest}';
var serverPublicDest = "server/public/";

gulp.task('server-public-watch', function () {
// start the livereload server
    livereload.listen();
    gulp.watch([watchGlob], function (files) {
        gulp.src(watchGlob)
            .pipe(gulp.dest(serverPublicDest))
            .pipe(livereload());
    });
});

gulp.task('default', ['sass', 'browserify', 'templates', 'manifest', 'webserver', 'livereload', 'watch', 'server-public-watch']);
