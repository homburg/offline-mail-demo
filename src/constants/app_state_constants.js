var mirror = require('key-mirror');

module.exports = mirror({
    APP_STATE_LOCAL_DB_CONNECTED: null,
    APP_STATE_IS_ONLINE: null,
    APP_STATE_IS_OFFLINE: null,
    APP_STATE_START_SYNC: null,
    APP_STATE_STOP_SYNC: null
});
