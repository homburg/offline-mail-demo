var AppDispatcher = require('../dispatcher/app_dispatcher');
var MailConstants = require('../constants/mails_constants');

var MailActions = {
  create: function(mail, storeName) {
    AppDispatcher.dispatch({
      actionType: MailConstants.MAIL_CREATE,
      mail: mail,
      storeName: storeName
    });
  },
  update(id, mail){
    AppDispatcher.dispatch({
      actionType: MailConstants.MAIL_UPDATE,
      id: id,
      mail: mail
    });
  }
};

window.mailActions = MailActions;

module.exports = MailActions;
