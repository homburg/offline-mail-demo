module.exports = function( server, app ) {
  return function( method, model, options ) {

    var promise = undefined,
      success = options.success,
      sync = {
        update: function() {
          var tableName = model.tableName || model.collection && model.collection.tableName,
            data = model.toJSON();
          data.updatedAt = Date.now();

          promise = server[ tableName ].query().filter( 'id', model.id ).modify( data ).execute();
        },
        read: function() {
          promise = server[ model.tableName ].query().filter(function(item){
            return item.deleted !== true;
          } ).execute();
        },
        "delete": function() {
          var tableName = model.tableName || model.collection && model.collection.tableName,
            data = model.toJSON();

          data.deleted = true;
          data.updatedAt = Date.now();

          promise = server[ tableName ].query().filter( 'id', model.id ).modify( data ).execute();
        },
        create: function() {
          var tableName = model.tableName || model.collection && model.collection.tableName,
            data = model.toJSON();
          data.updatedAt = Date.now();


          promise = server.add( tableName, data );
        }
      },
      syncMethodToUse = sync[ method ];

    syncMethodToUse && syncMethodToUse();

    promise.then( success );

    /*
    if( method !== "read" ){
      app.commands.execute('start:sync');
    }*/

    return promise;
  };
};


