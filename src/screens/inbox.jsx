var React = require('react'),
    RouteHandler = require('react-router').RouteHandler,
    MailList = require('../components/mail_list.jsx');

module.exports = React.createClass({
    
	render(){
		return (
			<div className='mail-store__container'>
                <div className='mail-store__list'>
                    <MailList storeName={this.props.params.store}/>
                </div>
                <div className='mail-store__viewer'>
    				<RouteHandler/>
                </div>
			</div>
		       )
	}
});
