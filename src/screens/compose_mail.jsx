var React = require('react'),
    Router = require('react-router'),    
    Link = Router.Link,
    RouteHandler = Router.RouteHandler,
    Api = require('../services/api.js'),
    MailsStore = require('../stores/mail_store.js'),
    LocalStore = require('../services/local_db.js'),
    AppStateStore = require('../stores/app_state_store.js'),
    DropZone = require('../components/dropzone.jsx'),
    request = require('request'),
    FilePreviewer = require('../components/file_previewer.jsx'),
    md5 = require('MD5');

function getMailState(id, storeName = 'drafts') {
	return {
		mail: MailsStore.get(id, storeName),
        attachments: []
	};
}


module.exports = React.createClass({
    _sendToStore(storeName){
         var recipient = React.findDOMNode(this.refs.recipient).value.trim(),
            subject = React.findDOMNode(this.refs.subject).value.trim(),
            message = React.findDOMNode(this.refs.message).value.trim(),
            sender = AppStateStore.currentUser().email,
            attachments = this.state.attachments,
            storedIn = storeName;

         LocalStore.createMail({recipient, subject, message, sender, storedIn, attachments});
    },

    getInitialState: function() {
        var {mailId} = this.props.params;
        if( mailId ){
            let store = 'drafts';
		    return getMailState(mailId, store);
        }
        return {
            attachments: []
        };
	},

    componentWillReceiveProps(nextProps){
        if( nextProps.storeName !== this.props.storeName ){
            this.setState(getMailState(this.props.params.mailId));
        }
    },
    /*
    componentDidMount() {
        MailsStore.addChangeListener(this._onChange);
    },

    componentWillUnmount() {
        MailsStore.removeChangeListener(this._onChange);
    },

    _onChange: function() {
        //this.setState(MailsStore.getAll(this.props.storeName));
    },*/


    _onSubmit(e){
        e.preventDefault();
         e.stopPropagation();
        
        this._sendToStore('outbox');
    },
    
    _saveToDraft(e){
        e.preventDefault();
        e.stopPropagation();
        this._sendToStore('drafts');
    },

    _onDrop(files){
        files.forEach(this._handleFile);    
    },

    _handleFile(file){
        var read = new FileReader();

        read.onloadend = () => {
                var filecontent = read.result,
                    checksum = md5(filecontent),
                
                    data = {
                        filecontent: filecontent,
                        filetype: file.type,
                        filesize: file.size,
                        filename: file.name,
                        checksum: checksum
                    },

                    state = this.state;

                state.attachments.push(data);

                this.setState(state);
        };

        read.readAsDataURL(file);
    },

    render(){
        var {recipient, subject, message} = this.state.mail || "",
            {attachments} = this.state;
        

            if(attachments) {
                attachments = attachments.map((attachment) => {
                    return <FilePreviewer className='compose-mail__attachment' file={attachment}/>
                });
            }


        
        return (
            <div className='compose-mail'>
            <h1 className='compose-mail__header'>Compose</h1>
            <form className='compose-mail__form' onSubmit={this._onSubmit}>
                <label className='compose-mail__label'>To
                    <input ref="recipient" placeholder="something@bla.com"  className='compose-mail__recipient' value={recipient}/>
                </label>    
                <label className='compose-mail__label'>Subject
                    <input ref="subject" placeholder="Subject" className='compose-mail__subject' value={subject}/>
                </label>
                <label className='compose-mail__label compose-mail__label--message'>Message
                    <textarea ref="message" className='compose-mail__message'>{message}</textarea>
                </label>   
            <DropZone className='compose-mail__dropzone' onDrop={this._onDrop}>
                <div className='compose-mail__attachments'>
                    {attachments}
                </div>
            </DropZone>
            <div className='compose-mail__buttons'>
                <button  className='compose-mail__submit-button' type="submit">Send</button>
                <button  className='compose-mail__save-draft-button' onClick={this._saveToDraft}>Save to drafts</button>
            </div>
            </form>
            </div>
        )
    }
});







