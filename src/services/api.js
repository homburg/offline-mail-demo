var API_URL = '/api';
var TIMEOUT = 10000;
var ApiConstants = require('../constants/api_constants.js');
var _pendingRequests = {};
var AppDispatcher = require('../dispatcher/app_dispatcher.js');
var request = require('superagent');


function abortPendingRequests(key) {
    if (_pendingRequests[key]) {
        _pendingRequests[key]._callback = function(){};
        _pendingRequests[key].abort();
        _pendingRequests[key] = null;
    }
}

function token() {
    return ""; //UserStore.getState().token;
}

function makeUrl(part) {
    return API_URL + part;
}

function dispatch(key, response, params) {
    var payload = {actionType: key, response: response};
    if (params) {
        payload.queryParams = params;
    }
    AppDispatcher.dispatch(payload);
}

// return successful response, else return request Constants
function makeDigestFun(key, params) {
    return function (err, res) {
        if (err && err.timeout === TIMEOUT) {
            dispatch(key, ApiConstants.REQUEST_TIMEOUT, params);
        } else if (res.status === 400) {
            //UserActions.logout();
        } else if (!res.ok) {
            dispatch(key, ApiConstants.REQUEST_ERROR, params);
        } else {
            dispatch(key, JSON.parse(res.text), params);
        }
    };
}

// a get request with an authtoken param
function get(url) {
    return request
        .get(url)
        .timeout(TIMEOUT)
        //.query({authtoken: token()});
}


function post(url, data) {
    return request
        .post(url)
        .send(data)
        .set('Accept', 'application/json')
        .timeout(TIMEOUT)
        //.query({authtoken: token()});
}

function put(url, data) {
    return request
        .put(url)
        .send(data)
        .set('Accept', 'application/json')
        .timeout(TIMEOUT)
        //.query({authtoken: token()});
}


var Api = {
    getStoreData(storeName, entityId) {
        var url = makeUrl("/"+storeName+".json");
        var params = {storeName};

        if( entityId ){
            url = makeUrl("/"+storeName+"/" + entityId +".json");
            params.entityId = entityId;
        }


        var key = ApiConstants.API_GET_EMAILS;
        
        abortPendingRequests(key);
        dispatch(key, ApiConstants.REQUEST_PENDING, params);

        _pendingRequests[key] = get(url).end(
            makeDigestFun(key, params)
        );
    },

    createMail(storeName, mail){
        var url = makeUrl("/"+storeName+".json");
        var params = {storeName};
        var key = ApiConstants.API_CREATE_EMAIL;
       
        
        abortPendingRequests(key);
        dispatch(key, ApiConstants.REQUEST_PENDING, params);

        _pendingRequests[key] = post(url, mail).end(
            makeDigestFun(key, params)
        );
    },

    updateDraftMail(remoteId, mail){
        var url = makeUrl("/"+storeName+"/"+remoteId+".json");
        var params = {storeName: 'drafts', mail: mail, remoteId: remoteId};
        var key = ApiConstants.API_UPDATE_EMAIL;
       
        
        abortPendingRequests(key);
        dispatch(key, ApiConstants.REQUEST_PENDING, params);

        _pendingRequests[key] = put(url, mail).end(
            makeDigestFun(key, params)
        );
    }

};

module.exports = Api;
