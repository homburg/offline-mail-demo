var Q = require('q'),
    request = require('superagent');

var ajax = function(method, url, data, stringifyData){
  var deferred = Q.defer();
  var req = request[method](url);

  if( method === 'get' ){
    req = req.query(data);
  } else {
    req = req.send(data);
  }

  req
    .timeout(10000)
    .set('Accept', 'application/json')
    .end((err, res) => {
        return deferred.resolve( JSON.parse(res.text) );
    });

  return deferred.promise;
}

module.exports = {

  get: function(url, data){
    return ajax('get', url, data)
  },

  put: function(url, data){
    return ajax('put', url, data, true)
  },

  post: function(url, data){
    return ajax('post', url, data, true)
  },

  remove: function(url, data){
    return ajax('delete', url, data, true)
  }

}
