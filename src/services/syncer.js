var startSync = require('./syncer_tasks/perform_sync.js');
var AppStateActions = require('../actions/app_state_actions');
var isSyncing = false;


var Syncer = {

    sync({dbServer}) {
        if( !isSyncing) {
            let syncInfos = [ {
              host: '/',
              path: 'api/mails',
              tableName: 'mails'
            } ];
            isSyncing = true;
            AppStateActions.startSync();
            startSync( {dbServer, syncInfos} ).finally( function() {
                isSyncing = false;
                AppStateActions.finishSync();
            } )
        }
    }

};

window.Syncer = Syncer;

module.exports = Syncer;



