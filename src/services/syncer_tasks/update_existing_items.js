var Q = require( 'q' ),
  _ = require( 'underscore' );

module.exports = function updateExistingItems( dbServer, syncConfig ) {
  return function(syncInfo) {
    var deferred = Q.defer(),
      remoteUpdatesMap = _.clone( syncInfo.remoteUpdatesMap );

    dbServer[syncConfig.tableName].query( 'remoteId' ).filter( function( item ) {
      return !_.isUndefined( remoteUpdatesMap[ item.remoteId ] )
    } ).execute().then( function( existingItems ) {
      var promises = [];
      
      existingItems.forEach( function( item ) {
        var deferred = Q.defer();
        dbServer[syncConfig.tableName].query().filter( 'remoteId', item.remoteId ).modify( remoteUpdatesMap[ item.remoteId ] ).execute().then( function() {
          delete remoteUpdatesMap[ item.remoteId ];
          deferred.resolve();
        } );

        promises.push( deferred.promise );
      } );

      return Q.all( promises );
    } ).then( function() {
      syncInfo.remoteUpdatesMap = remoteUpdatesMap;

      deferred.resolve( syncInfo)
    } );

    return deferred.promise;
  }
};
