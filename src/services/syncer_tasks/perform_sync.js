var Marionette = require( 'backbone.marionette' ),
  Q = require( 'q' ),
  _ = require( 'underscore' ),
  ajax = require( '../ajax.js' ),
  updateSyncInfo = require( './update_sync_info.js' ),
  updateExistingItems = require( './update_existing_items.js' ),
  pushNewItems = require( './push_new_items.js' ),
  pushChangedItems = require( './push_changed_items.js' ),
  addNewItems = require( './add_new_items.js' ),
  removeDeletedItems = require( './remove_deleted_items.js' ),
  getSyncInfo = require( './get_sync_info.js' ),
  normalizeRemoteDataAndPrepareDataStructures = require('./normalize_remote_data_and_prepare_data_structures.js');

module.exports = function startSync( options = {}) {
  var {dbServer} = options,
      syncConfigurations = options.syncInfos ,
      deferred = Q.defer();
  try {
    getSyncInfo( dbServer ).then( function( syncInfo ) {
      var promises = [];
      
      syncConfigurations.forEach( function( syncConfig ) {
        var syncDeferred = Q.defer();
        promises.push(syncDeferred.promise);
        
        ajax.get( syncConfig.host+syncConfig.path+".json", { updatedAt: syncInfo.updatedAt } )
          .then( normalizeRemoteDataAndPrepareDataStructures( syncInfo ) )
          .then( updateExistingItems( dbServer, syncConfig ) )
          .then( addNewItems( dbServer, syncConfig ) )
          .then( pushNewItems( dbServer, syncConfig ) )
          .then( pushChangedItems( dbServer, syncConfig ) )
          .then( removeDeletedItems( dbServer, syncConfig ) )
          .catch( function( e ) {
            console.error( "Exception while syncing: ", e );
          } )
          .finally( function() {
            syncDeferred.resolve( syncInfo );
          } );
      } )

      Q.all(promises )
        .then( updateSyncInfo( dbServer ) )
        .then(deferred.resolve);

    } )
  } catch( e ) {
    console.error( "Exception while syncing: ", e );
  }

  return deferred.promise;
};
