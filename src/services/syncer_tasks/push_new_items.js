var Q = require( 'q' ),
  _ = require( 'underscore' ),
  ajax = require( '../ajax.js' );

module.exports = function pushNewItems( dbServer, syncConfig ) {
  return function(syncInfo) {

    var deferred = Q.defer();
    dbServer[syncConfig.tableName].query().filter( function( item ) {
      return item.remoteId === undefined;
    } )
      .execute()
      .then( function( results ) {
        var promises = [];
        results.forEach( function( newItem ) {
          var deferred = Q.defer();

          promises.push( deferred.promise );

          ajax.post( syncConfig.host+syncConfig.path+".json", newItem )
            .then( function( data ) {
              data.remoteId = data._id;
              delete data._id
              data.updatedAt = new Date( data.updatedAt ).getTime();

              dbServer[syncConfig.tableName].query().filter( 'id', newItem.id ).modify( data ).execute().then( function() {
                deferred.resolve( data );
              } );
            } );

        } )
        return Q.all( promises );
      } ).then( function(newData){
        syncInfo.newItemsPushedToServer = newData;
        deferred.resolve(syncInfo);
      } );
    return deferred.promise;
  }
};
