var Q = require( 'q' ),
  _ = require( 'underscore' ),
  ajax = require( '../ajax.js' );

module.exports = function pushChangedItems( dbServer, syncConfig ) {

  return function( syncInfo ) {
    var deferred = Q.defer(),
      pushedItems = syncInfo.newItemsPushedToServer,
      pushedItemRemoteId = _.pluck( pushedItems, 'remoteId' ),
      remoteUpdatedIds = Object.keys( syncInfo.remoteUpdatesMap );

    dbServer[syncConfig.tableName].query( 'updatedAt' )
      .lowerBound( syncInfo.updatedAt )
      .execute()
      .then( function( results ) {
        var promises = [];

        results.forEach( function( changedItem ) {
          var deferred = Q.defer(),
            remoteId = changedItem.remoteId;
            
          if( remoteId && pushedItemRemoteId.indexOf( remoteId ) === -1 && remoteUpdatedIds.indexOf( remoteId ) === -1 ) {
            promises.push( deferred.promise );

            ajax.put( syncConfig.host + syncConfig.path + "/" + remoteId + ".json", changedItem )
              .then( function( data ) {
                data.remoteId = data._id;
                delete data._id

                data.updatedAt = new Date( data.updatedAt ).getTime();

                dbServer[syncConfig.tableName].query().filter( 'id', changedItem.id ).modify( data ).execute().then( function() {
                  deferred.resolve( data );
                } );
              } );

          }

        } );

        return Q.all( promises );
      } ).then( function( changedItems ) {
        syncInfo.changedItemsPushedToServer = changedItems;
        deferred.resolve( syncInfo );
      } );


    return deferred.promise;
  }
};
