var Q = require('q');

module.exports = function removeDeletedItems( dbServer, syncConfig ) {
  return function(syncInfo) {
    var deferred = Q.defer();

    dbServer[syncConfig.tableName].query().filter('deleted', true ).execute()
      .then(function(itemsToRemove){
        var promises = [];
        itemsToRemove.forEach(function(item){
          var deferred = Q.defer();

          dbServer[syncConfig.tableName].remove(item.id ).then(deferred.resolve);

          promises.push(deferred.promise)
        });

        Q.all(promises ).then(function(){
          syncInfo.removedItems = itemsToRemove;
          deferred.resolve(syncInfo);
        });
      });

    return deferred.promise;
  }
}
