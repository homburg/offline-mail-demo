var React = require('react'),
    Link = require('react-router').Link;

function updateEnabledState(storeMethod){
    return {enabled: storeMethod().isOnline};
}

module.exports = React.createClass({
    
    getInitialState: function() {
        if( this.props.storeMethod) {
		    return updateEnabledState( this.props.storeMethod );
        }
        return {enabled: true};
	},


    componentDidMount() {
        if( this.props.store ){
		    this.props.store.addChangeListener( this._onChange );
        }
	},

	componentWillUnmount() {
        if( this.props.store ){        
    		this.props.store.removeChangeListener( this._onChange );
        }
	},

	_onChange: function() {
		this.setState( updateEnabledState(this.props.storeMethod));
	},

    render(){
        var className = 'header-button';

        if( !this.state.enabled ){
            className = className + ' header-button--disabled';
        }

        if( this.props.to ){
            return (
                <Link to={this.props.to} className={className} activeClassName='header-button--active' disabled={!this.state.enabled} >{this.props.title}</Link>
            )
        } else {
            if( this.state.enabled ){
                return <div className={className} onClick={this.props.onClick}>{this.props.title}</div>
            } else {
                return <div className={className}>{this.props.title}</div>                
            }
        }
    }
});
