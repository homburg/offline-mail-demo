var React = require('react'),
    ConnectionStatus = require('./connection_status.jsx'),
    HeaderButton = require('./header_button.jsx'),
    AppStateStore = require('../stores/app_state_store.js'),
    Syncer = require('../services/syncer.js');



module.exports = React.createClass({
    

    _synchronize(){
        
        Syncer.sync(AppStateStore.localDBServer());
    },

    render(){
        
        return (
            <header className='header'>
                	<ConnectionStatus className="header-button" />
                    <HeaderButton title='Compose' to='compose'/>
                    <HeaderButton title='Sync' store={AppStateStore} storeMethod={AppStateStore.isOnline} onClick={this._synchronize}/>
            </header>
        )

/*
 
        return (
            <header className='header'>
                	<ConnectionStatus className="header-button" />
                    <HeaderButton title='Compose' to='compose'/>
            </header>
        )
       */
/*
       return (
            <header className='header'>
                    <HeaderButton title='Compose' to='compose'/>
            </header>
        )*/
    }

});
