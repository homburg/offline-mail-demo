var React = require( 'react' ),
	Router = require( 'react-router' ),
	Link = Router.Link,
	StoreListRow = require( './store_row.jsx' ),
	RouteHandler = Router.RouteHandler;

module.exports = React.createClass( {

	render(){
		var items = [ 'inbox', 'drafts', 'outbox', 'sent', 'trash' ].map( ( name ) => <StoreListRow name={name}/> );

		return (
			<nav className='store-list'>
				{items}
			</nav>
		)
	}
} );





