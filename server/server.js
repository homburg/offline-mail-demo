// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var cors = require('cors')
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test');

var Mail = mongoose.model('Mail', { 
    recipient: String, 
    sender:String, 
    date:Date, 
    message:String, 
    subject: String, 
    storedIn:String, 
    hasBeenRead:Boolean,
    updatedAt: Date,
    attachments: [{
        checksum: String,
        filecontent: String,
        filename: String,
        filesize: Number,
        filetype: String,

    }]
});

Mail.find({}).remove().exec();

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyParser.json({limit: '50mb'}));
app.use(express.static('public'));
app.use(cors());

var port = process.env.PORT || 8000;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router


router.get('/ping.json', function(req, res){
    res.json({message: 'ok'});
});

/*
// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/:storeName.json', function(req, res) {
    Mail.find({storedIn: req.params.storeName}, function(err, mails) {
            if (err)
                res.send(err);

            res.send(mails.map(function(mail) {
                return mail;                 
            }));
        });
});

router.post('/:storeName.json', function(req, res) {
    if( req.params.storeName === 'outbox' || req.params.storeName === 'drafts' ){
        var mail = new Mail();      // create a new instance of the Bear model
        mail.subject = req.body.subject; 
        mail.recipient = req.body.recipient; 
        mail.sender = req.body.sender; 
        mail.message = req.body.message; 
        mail.storedIn = req.params.storeName;
        mail.updatedAt = new Date();
        
        mail.save(function(err){
            if( err ){
                console.error(err);
                res.send(err);
            } 
            console.log("mail added to ", mail.storedIn, " for processing: ", mail);
            res.json(mail);
        });   
    } else {
        res.send({message: 'wrong store'})
    }
});
*/
// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/mails.json', function(req, res) {
    Mail.find({}, function(err, mails) {
            if (err)
                res.send(err);

            res.send(mails.map(function(mail) {
                return mail;                 
            }));
        });
});

router.post('/mails.json', function(req, res) {
    if( req.body.storedIn === 'outbox' || req.body.storedIn === 'drafts' ){
        var mail = new Mail();      // create a new instance of the Bear model
        mail.subject = req.body.subject; 
        mail.recipient = req.body.recipient; 
        mail.sender = req.body.sender; 
        mail.message = req.body.message; 
        mail.storedIn = req.body.storedIn;
        mail.updatedAt = new Date();
        mail.attachments = req.body.attachments;
        
        mail.save(function(err){
            if( err ){
                console.error(err);
                res.send(err);
            } 
            console.log("mail added to ", mail.storedIn, " for processing: ", mail.subject);
            res.json(mail);
        });   
    } else {
        res.send({error: 'wrong store'})
    }
});


router.put('/mails/:_id.json', function(req, res) {
    Mail.findById(req.params._id, function(err, mail){      // create a new instance of the Bear model
        if( err ){
            res.send(err);
        }
       
        if( !mail ){
            res.send({error: '404'})
            return;
        } 
        mail.subject = req.body.subject; 
        mail.recipient = req.body.recipient; 
        mail.sender = req.body.sender; 
        mail.message = req.body.message; 
        mail.updatedAt = Date.now();
        mail.deleted = req.body.deleted;
        mail.attachments = req.body.attachments;

        mail.save(function(err){
            if( err ){
                console.error(err);
                res.send(err);
            } 
            console.log("mail added to ", mail.storedIn, " for processing: ", mail.subject);
            res.json(mail);
        });   
    });
});

var timeout = setInterval(function(){
    Mail.find({storedIn: 'outbox'}, function(err, mails){
        if( err) {
            console.log('err when finding outbox mails: ', err);
        }
        mails.forEach(function(mail){
            mail.storedIn = "sent";
            mail.date = new Date();
            mail.save(function(err){
                if(!err){
                    console.log('mail '+ mail.subject + ' sent');

                    if( mail.recipient === 'jakob@npapps.com' ){
                        var newMail = new Mail();
                        
                        newMail.subject = mail.subject; 
                        newMail.recipient = mail.recipient; 
                        newMail.sender = mail.sender; 
                        newMail.message = mail.message; 
                        newMail.updatedAt = Date.now();
                        newMail.date = Date.now();
                        newMail.deleted = false;
                        newMail.attachments = mail.attachments;
                        newMail.storedIn = 'inbox';

                        newMail.save(function(err){
                            if(!err){
                                console.log('mail '+ newMail.subject + ' recieved in inbox');
                            } else {
                                console.log('save error newMail; ', err);
                            }
                        })




                    }
                }
            });
        });
    });

}, 1000);


// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
